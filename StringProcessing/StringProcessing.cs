﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    static public class StringProcessing
    {
        public class ReverseComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                // Compare y and x in reverse order.
                
                return 
                    
                    String.Compare(y, x);

            }
        }

        static public string SortStrings(string s,string check)
        {
            s = s.ToUpper();
            s.Replace("  ", " ");
            check = check.ToUpper();
            string [] arr= s.Split(' ');
            
            string s1="";
            string s2="";
            for(int i = 0;i<arr.Length; i++)
            {
                string try_me= arr[i];
                if (try_me.EndsWith(check))
                    s1 += (try_me)+' ';
                else s2 += try_me+' ';
            }
            string[] first_group= s1.Split(' ');
            ReverseComparer g = new ReverseComparer() ;
            Array.Sort(first_group,g);
            string[] second_group = s2.Split(' ');
            Array.Sort(second_group,g);
            string res = "";
            for(int i =0;i<first_group.Length;i++)
            {
                res += first_group[i] +' ' ;

            }
            for (int i = 0; i < second_group.Length; i++)
            {
                res += second_group[i] + ' ';
            }

            return res;
        }
    }
}
